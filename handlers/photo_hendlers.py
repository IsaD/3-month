from aiogram import types

from config import dp


# @dp.message_handler(commands=["pic"])
async def send_picture(message: types.Message):
    """Отправляет картинку пользователю"""
    # Doc String
    photo1 = open('images/Super_Cars_HD_Wallpaper.jpg', 'rb')
    await message.answer_photo(photo1, caption="Машина")