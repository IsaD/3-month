from aiogram import types
from handlers.constants import HELLO_TEXT




# @dp.message_handler(commands=["start"])
async def start(message: types.Message):
    print(f"{message.from_user=}")
    kb = types.InlineKeyboardMarkup()
    kb.add(
        types.InlineKeyboardButton(
            "Наш сайт",
            url="https://mashina.kg/"
        ),
        types.InlineKeyboardButton(
            "О нас",
            callback_data='about'
        )
    )
    kb.add(types.InlineKeyboardButton(
        "Истории успеха",
        callback_data='success_story')
    )
    await message.reply(HELLO_TEXT, reply_markup=kb)



# @dp.message_handler()
async def echo(message: types.Message):
    """Эхо, функция, которая отправляет пользоватео"""
    await message.answer(message.text)