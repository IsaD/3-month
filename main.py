from aiogram import executor
from config import dp
import logging
from handlers.photo_hendlers import send_picture
from handlers.start import start, echo

if __name__ == "main":
    logging.basicConfig(level=logging.INFO)
    dp.register_message_handler(send_picture, commands=["picture"])
    dp.register_message_handler(start, commands=["start"])
    dp.register_message_handler(echo)
    executor.start_polling(dp)